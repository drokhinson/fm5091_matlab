function [ vol ] = volCalc_Newton( So, K, R, T, OP )
    % volCalc_Newton calculates the implied volatility of an option
    % using the Newton method.
    %
    %   function [ vol ] = volCalc_Newton( So, K, R, T, Price )
    %   
    %   volCalc_Newton takes the following inputs:
    %     Underlying Spot price (So) $
    %     Strike Price (K) $
    %     Risk Free Rate (R) decimal
    %     Tenor (T) in years
    %     Option Price (OP) $
    %
    %   Example Input:
    %   volCalc_Newton(50,50,0.05,1,10);
    %   volCalc_Newton(50*ones(10,1),50*ones(10,1),0.05*ones(10,1),
    %       ones(10,1),randn(10,1)+10.89);

    % creating vectors with initial guess of vol
    % initial vol guess: 200%
    size = length(OP);
    volGuess = 2*ones(size,1);
    % initialize counter to track num of steps in approximation
    counter = zeros(size,1);
    
    % estimation funciton
    [vol, i]  = estimateVol(OP, So, K, R, T, ...
        volGuess, counter);

    % Display Output
    fprintf('Volatility of Option w/ inputs \n');
    fprintf('(Price, Spot, Strike, Rate, Tenor)\n');
    for c = 1:length(OP)
    fprintf('(%.3f, %.1f, %.1f, %.2f, %.1f) ', OP(c), So(c), K(c), R(c), T(c));
    fprintf('Volatility = %.3f\n', vol(c));
    fprintf('Steps in approx. = %.0f\n', i(c));
    end
end

function [ vol, i_final ] = estimateVol ( OP, So, K, R, T, v_guess, i)
    %recurrsivly estimates the volatility of a option using Newton's method
    err = 0.0001; %sets degree of accuracy for approximation
    
    %calculate option sensetivity to vol (Vega)
    op_Vega = CalcVega(So, K, R, T, v_guess);
    op_Price = CalcBSPrice(So, K, R, T, v_guess);

    %update vol guess after comparing desired option price to the 
    %calculated option price at guessed volatility
    for c = 1:length(OP)
        if(abs(1 - OP(c)./op_Price(c)) < err)
            %if option price is within desired precision stop bisection by
            %setting both upper and lower vol guess to the midpoint
            v_guess(c) = v_guess(c);
            i(c) = i(c);
        else
            %adjust guess by d(vol) = d(optionPrice)/optionVega
            dx = (OP(c) - op_Price(c)) ./ op_Vega(c);
            v_guess(c) = v_guess(c) + dx;
            i(c) = i(c) + 1;
        end
    end
    
    %recurse through function until all options being evaluated are within
    %desired precision or until program iterates 100 time through the
    %approximation
    if(max(abs(1 - OP./op_Price)) < err || max(i) >= 100)
        vol = v_guess;
        i_final = i;
    else
        [vol, i_final] = estimateVol ( OP, So, K, R, T, v_guess, i);
    end
end

function [ price ] = CalcBSPrice (So, K, R, T, Vol)
    % Calculate Black Scholes price of an option
    d1 = Calc_d1(So, K, R, T, Vol);
    d2 = Calc_d2(So, K, R, T, Vol);
    price = So .* normcdf(d1) - K .* exp(-R .* T) .* normcdf(d2);
end

function [ vega ] = CalcVega (So, K, R, T, Vol)
    % Calculate Black Scholes vol sensitivity (vega) of an option
    d1 = Calc_d1(So, K, R, T, Vol);
    vega = So .* normpdf(d1) .* sqrt(T);
end

function [ d1 ] = Calc_d1 (So, K, R, T, Vol)
    d1 = (log(So ./ K) + (R + Vol.^2 ./ 2) .* T) ./ (Vol .* sqrt(T));
end

function [ d2 ] = Calc_d2 (So, K, R, T, Vol)
    d2 = Calc_d1(So, K, R, T, Vol) - Vol .* sqrt(T);
end