function [ vol ] = volCalc_Bisection( So, K, R, T, OP )
    % volCalc_Bisection calculates the implied volatility of an option
    % using the bisection method.
    %
    %   function [ vol ] = volCalc_Bisection( So, K, R, T, Price )
    %   
    %   volCalc_Bisection takes the following inputs:
    %     Underlying Spot price (So) $
    %     Strike Price (K) $
    %     Risk Free Rate (R) decimal
    %     Tenor (T) in years
    %     Option Price (OP) $
    %
    %   Example Input:
    %   volCalc_Bisection(50,50,0.05,1,10);
    %   volCalc_Bisection(50*ones(10,1),50*ones(10,1),
    %       0.05*ones(10,1),ones(10,1),randn(10,1)+10.89)

    % creating vectors with initial upper and lower guesses for the vol
    % initial upper guess: 200%
    % initial lower guess: 0
    size = length(OP);
    volGuess_up = 2*ones(size,1);
    volGuess_down = zeros(size,1);
    % initialize counter to track num of steps in approximation
    counter = zeros(size,1);
    
    % estimation funciton
    [vol, i]  = estimateVol(OP, So, K, R, T, ...
        volGuess_up, volGuess_down, counter);

    % Display Output
    fprintf('Volatility of Option w/ inputs \n');
    fprintf('(Price, Spot, Strike, Rate, Tenor)\n');
    for c = 1:length(OP)
    fprintf('(%.3f, %.1f, %.1f, %.2f, %.1f) ', OP(c), So(c), K(c), R(c), T(c));
    fprintf('Volatility = %.3f\n', vol(c));
    fprintf('Steps in approx. = %.0f\n', i(c));
    end
end

function [ vol, i_final ] = estimateVol ( OP, So, K, R, T, v_up, v_down, i)
    %recurrsivly estimates the volatility of a option using bisection
    %approximation technique

    err = 0.001; %sets degree of accuracy for approximation
    v_mid = (v_up + v_down)/2;
    %calculate option price using the vol at the midpoint
    op_mid = CalcBSPrice(So, K, R, T, v_mid);
    
    %update lower and upper guesses for volatility after comparing desired 
    %option price to the calculated option price of the midpoint vol
    for c = 1:length(OP)
        if(abs(1 - OP(c)./op_mid(c)) < err)
            %if option price is within desired precision stop bisection by
            %setting both upper and lower vol guess to the midpoint
            v_up(c) = v_mid(c);
            v_down(c) = v_mid(c);
            i(c) = i(c);
        elseif(op_mid(c) > OP(c))
            v_up(c) = v_mid(c);
            i(c) = i(c) + 1;
        else
            v_down(c) = v_mid(c);
            i(c) = i(c) + 1;
        end
    end
    
    %recurse through function until all options being evaluated are within
    %desired precision or until program iterates 100 time through the
    %approximation
    if(max(abs(1 - OP./op_mid)) < err || max(i) >= 100)
        vol = v_mid;
        i_final = i;
    else
        [vol, i_final] = estimateVol ( OP, So, K, R, T, v_up, v_down, i);
    end
end

function [ price ] = CalcBSPrice (So, K, R, T, Vol)
    % Calculate Black Scholes price of an option
    d1 = Calc_d1(So, K, R, T, Vol);
    d2 = Calc_d2(So, K, R, T, Vol);
    price = So .* normcdf(d1) - K .* exp(-R .* T) .* normcdf(d2);
end

function [ d1 ] = Calc_d1 (So, K, R, T, Vol)
    d1 = (log(So ./ K) + (R + Vol.^2 ./ 2) .* T) ./ (Vol .* sqrt(T));
end

function [ d2 ] = Calc_d2 (So, K, R, T, Vol)
    d2 = Calc_d1(So, K, R, T, Vol) - Vol .* sqrt(T);
end