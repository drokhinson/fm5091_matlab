%Reset Workspace
clear
clc

%Provide user with preset paramters to use
disp('Initilizing blackScholes functions');
disp('Default Parameter Values');
disp('Spot (So) = $100');
disp('Strike (K) = $100');
disp('Rate (R) = 0.01');
disp('Time to Expiration (T) = 1 year');
disp('Volatility (Vol) = 0.01');
disp(' ');
disp('Copy Variables: (So,K,R,T,Vol)'); %save time typing
disp(' ');

%Initialize parameters and assigne default values
syms So
syms K
syms R
syms T
syms Vol

So = 100;
K = 100;
R = 0.01;
T = 1;
Vol = 0.01;

%Calculate Option Price and Greeks using Black Scholes

d1 = @(So, K, R, T, Vol) (log(So ./ K) + (R + Vol.^2 ./ 2) * T) ./ (Vol * sqrt(T));
d2 = @(So, K, R, T, Vol) d1(So, K, R, T, Vol) - Vol .* sqrt(T);

%Black-Scholes Option Price for Call and Put Options
bsCall = @(So, K, R, T, Vol) So .* normcdf(d1(So, K, R, T, Vol)) - K .* exp(-R * T) .* normcdf(d2(So, K, R, T, Vol));
bsPut = @(So, K, R, T, Vol) -So .* normcdf(-d1(So, K, R, T, Vol)) + K .* exp(-R * T) .* normcdf(-d2(So, K, R, T, Vol));

%BS Delta (sensitivity of price to change in underlying)
bsCall_Delta = @(So, K, R, T, Vol) normcdf(d1(So, K, R, T, Vol));
bsPut_Delta = @(So, K, R, T, Vol) normcdf(d1(So, K, R, T, Vol)) - 1;

%BS Gamma (sensitivity of Detla to change in underlying)
bs_Gamma = @(So, K, R, T, Vol) normpdf(d1(So, K, R, T, Vol)) ./ (So * Vol * sqrt(T));

%BS Vega (sensitivity of price to change in volatility)
bs_Vega =  @(So, K, R, T, Vol) So * normpdf(d1(So, K, R, T, Vol)) * sqrt(T);

%BS Theta (sensitivity of price to change in time to expiration)
bsCall_Theta = @(So, K, R, T, Vol) -(So * normpdf(d1(So, K, R, T, Vol)) * Vol)/(2 * sqrt(T)) - R*K*exp(-R*T)*normcdf(d2(So, K, R, T, Vol));
bsPut_Theta = @(So, K, R, T, Vol) -(So * normpdf(d1(So, K, R, T, Vol)) * Vol)/(2 * sqrt(T)) + R*K*exp(-R*T)*normcdf(-d2(So, K, R, T, Vol));

%BS Rho (sensitivity of price to change in rate)
bsCall_Rho = @(So, K, R, T, Vol) K*T*exp(-R*T)*normcdf(d2(So, K, R, T, Vol));
bsPut_Rho = @(So, K, R, T, Vol) -K*T*exp(-R*T)*normcdf(-d2(So, K, R, T, Vol));

