function [ price ] = CalcCRRPrice( AorE, CorP, Spot, Strike, ...
    RFRate, Tenor, Volatility, numSteps, Dividend, greekStep)
    % CalcCRRPrice calculates the price of an American or a Europen option
    % as well as the option greeks using using a biomial tree.
    %
    %   function [ output_args ] = CalcCRRPrice( AorE, CorP, Spot, Strike,
    %   RFRate, Volatility, Time, numSteps, Dividend, greekStep )
    %   
    %   CalcCRRPrice takes the following inputs:
    %     Option Type (AorE) - American or European (Input 'A' or 'E')
    %     Call or Put (CorP) - Call or Put (Input 'C' or 'P')
    %     Underlying Spot price (So) $
    %     Strike Price (K) $
    %     Risk Free Tate (RFRate) decimal
    %     Volatility (Vol) decimal
    %     Tenor (T) in years
    %     Number of steps (n)
    %     Dividend Yield (Div)
    %     GreekStep (gStep) - shock size for greek calculation
    %
    %   Example Input:
    %   CalcCRRPrice('E','C',50,50,0.05,1,0.5,4,0,0.001);

    % Cast variables as syms to simplify manipulation
    syms AE;
    syms CP;
    syms So;
    syms K;
    syms R;
    syms EffRate;
    syms Vol;
    syms dt;
    syms n;
    syms gStep;
    syms optionType;
    syms textType;

    AE = AorE;
    CP = CorP;
    So = Spot;
    K = Strike;
    R = RFRate;
    EffRate = RFRate - Dividend;
    n = numSteps;
    Vol = Volatility;
    dt = Tenor/n;
    gStep = greekStep;

    % Test to make sure string inputs are correct
    if (CP == 'C')
        optionType = 'Call';
    elseif (CP == 'P')
        optionType = 'Put';
    else
        disp('Invalid Input for Call or Put Option Type');
        disp('Please call function with "C" or "P" as inputs');
        return
    end
    
    % Test inputs
    if (gStep > 1) || (Dividend > 1) || (R > 1)
        disp('Invalid Inputs');
        disp('Greeks step, Risk Free Rate, Dividend Yeild must be > 1');
        return
    elseif (gStep < 0) || (Dividend < 0) || (Tenor < 0) || ...
            (So < 0) || (K < 0)
        disp('Invalid Inputs');
        disp('Greeks step, Dividend, Tenor, Spot, Strike must be > 0');
        return
    end
    
    %Calculate Option Price
    if (AE == 'A')
        textType = 'American';
        price = PriceAmericanOption(CP, So, K, R, EffRate, Vol, dt, n);
    elseif (AE == 'E')
        textType = 'European';
        price = PriceEuropeanOption(CP, So, K, R, EffRate, Vol, dt, n);
    else
        disp('Invalid Input for American or European Option Type');
        disp('Please call function with "A" or "E" as inputs');
        return
    end
    
    % Calculate Greeks
    delta = CalcDelta(AE, CP, So, K, R, EffRate, Vol, dt, n, gStep);
    gamma = CalcGamma(CP, So, K, R, EffRate, Vol, dt, n);
    rho = CalcRho(AE, CP, So, K, R, Dividend, Vol, dt, n, gStep);
    theta = CalcTheta(AE, CP, So, K, R, EffRate, Vol, dt, n);
    vega = CalcVega(AE, CP, So, K, R, EffRate, Vol, dt, n, gStep);
    
    % Display Output
    fprintf('For %s %s Option w/ inputs \n', textType, optionType);
    fprintf('(Spot, Strike, Rate, Tenor, Volatility, Div)\n');
    fprintf('(%.1f, %.1f, %.3f, %.2f, %.3f, %.3f)\n\n', So, K, R, Tenor, Vol, Dividend);
    fprintf('Option Price = %.3f\n', price);
    fprintf('Delta = %.3f\n', delta);
    fprintf('Gamma = %.4f\n', gamma);
    fprintf('Rho = %.3f\n', rho);
    fprintf('Theta = %.3f\n', theta);
    fprintf('Vega = %.3f\n\n', vega);
    
end

function [price] = PriceEuropeanOption(CP, So, K, R, EffRate, Vol, dt, n)
    % Create vector of final prices at expiration (not path dependant)
    PriceVector = BuildPriceVector(So, Vol, dt, n);
    P = CalcP(EffRate, Vol, dt);
    % Calculate the price of a European Option
    price = CalcEOptionPrice(CP, 1, PriceVector, P, K, R, dt);
end

function [price] = PriceAmericanOption(CP, So, K, R, EffRate, Vol, dt, n)
    % Generate matrix of prices up to expiration (path dependant)
    PriceMatrix = BuildPriceMatrix(So, Vol, dt, n);
    % Select final prices from Price matrix
    priceVector = flipud(diag(flipud(PriceMatrix),0));
    P = CalcP(EffRate, Vol, dt);
    % Calculate the price of a American Option
    price = CalcAOptionPrice(CP, 0, PriceMatrix, P, K, R, dt, priceVector);
end

function [priceVector] = BuildPriceVector(So, vol, dt, n)
    % Generates vector of final prices after specified number of steps
    priceVector = zeros(n+1,1);
    priceVector(1) = So * UpStep(vol, dt) ^ n;
    j = 2;
    for i = 2:n+1
        priceVector(i) = priceVector(1) * DownStep(vol,dt) ^ (j);
        j = j + 2;
    end
end

function [priceVector] = BuildPriceMatrix(So, vol, dt, n)
    % Generates underlying price matrix for
    priceVector = zeros(n+1,n+1);
    priceVector(1,1) = So;
    
    for u = 1:n+1
        for d = 1:n+1
            priceVector(d,u) = priceVector(1) * UpStep(vol,dt) ^ u ...
                * DownStep(vol,dt) ^ d;
        end
    end
end

function [up] = UpStep(vol, dt )
    up = exp(vol * sqrt(dt));
end

function [down] = DownStep(vol, dt)
    down = exp(-vol * sqrt(dt));
end

function [P] = CalcP(EffRate, vol, dt)
    % Calculate probability of stock price moving up
    P = (exp(EffRate * dt) - DownStep(vol, dt)) / (UpStep(vol, dt) - ...
        DownStep(vol, dt));
end

function [payoff] = CalcPayoff(CP, SpotLevel, Strike)
% Calculates payoff based on whether option is call or put
    if CP == 'C'
        payoff = SpotLevel - Strike;
    else
        payoff = Strike - SpotLevel;
    end
    
    if payoff < 0
        payoff = 0;
    end
end

function [OptionPrice] = CalcEOptionPrice(CP, init, priceVector, P,...
    K, R, dt)
    % Calculates the price of European option
    
    % opVector contains the opPrices for the next time step working back
    % from final day to option inception day
    opVector = zeros(length(priceVector)-1,1);
    for i = 1: length(priceVector)-1
        % init acts as a bool to determine if input is passing
        % initial stock prices or calculated option prices
        if(init == 1)
            opVector(i) = exp(-R * dt)* ...
                (P * CalcPayoff(CP, priceVector(i), K)...
                +(1-P)*CalcPayoff(CP, priceVector(i+1), K));
        else
            opVector(i) = exp(-R * dt)* ...
                (P * priceVector(i) + (1-P)* priceVector(i+1));
        end
    end

    % recursivly iterate through the function until tree converges to final
    % day of inception
    if(length(opVector) > 2)
        OptionPrice = CalcEOptionPrice(CP, 0, opVector, P, K, R, dt);
    else
        OptionPrice = exp(-R * dt)* ...
            (P * opVector(1) + (1-P)* opVector(2));
    end
end

function [OptionPrice] = CalcAOptionPrice(CP, rCount, priceMatrix, P,...
    K, R, dt, priceVector)
    % Calculates the price of American option
    
    % opVector contains the opPrices for the next time step working back
    % from final day to option inception day
    opVector = zeros(length(priceVector)-1,1);
    for i = 1: length(priceVector)-1
        % init acts as a bool to determine if input is passing
        % initial stock prices or calculated option prices
        if(rCount == 0)
            opVector(i) = exp(-R * dt)* ...
                (P * CalcPayoff(CP, priceVector(i), K)...
                +(1-P)*CalcPayoff(CP, priceVector(i+1), K));
        else
            opVector(i) = exp(-R * dt)* ...
                (P * priceVector(i) + (1-P)* priceVector(i+1));
        end
    end
    
    rCount = rCount + 1; %add one to recursion counter
    nextPrices = flipud(diag(flipud(priceMatrix),rCount));
    % compare option prices in opVector to payoff at next time set to
    % determine if early excersise is desirable
    for i = 1:length(opVector)
        % select next time step prices from priceMatrix using recursion
        % counter
        opVector(i) = max(CalcPayoff(CP, nextPrices(i), K), opVector(i));
    end
    
    % recursivly iterate through the function until tree converges to final
    % day of inception
    if(length(opVector) > 2)
        OptionPrice = CalcAOptionPrice(CP, rCount, priceMatrix, P,...\
            K, R, dt, opVector);
    else
        OptionPrice = exp(-R * dt)* ...
            (P * opVector(1) + (1-P)* opVector(2));
    end
end

function [Delta] = CalcDelta(AE,CP, So, K, R, EffRate, Vol, dt, n, gStep)
    % Shock spot price up and down by the given gStep amount
    S1 = So*(1+gStep);
    S2 = So*(1-gStep);
    
    if (AE == 'A') 
        price1 = PriceAmericanOption(CP, S1, K, R, EffRate, Vol, dt, n);
        price2 = PriceAmericanOption(CP, S2, K, R, EffRate, Vol, dt, n);
    else
        price1 = PriceEuropeanOption(CP, S1, K, R, EffRate, Vol, dt, n);
        price2 = PriceEuropeanOption(CP, S2, K, R, EffRate, Vol, dt, n);
    end
    
    Delta = (price2 - price1)/(S2-S1);
end

function [Gamma] = CalcGamma(CP, So, K, R, EffRate, vol, dt, n)
    % Calculate Gamma via linear approximation
    
    % Calculate option prices for gamma evaluation using same biomial
    % recursive method as used for option price calulation.
    underlyingPriceVector = BuildPriceVector(So, vol, dt, n);
    P = CalcP(EffRate, vol, dt);
    gVector = CalcGammaVector(CP, 1, underlyingPriceVector, P, K, R, dt);
    
    % Calculate underlying prices for gamma evaluation
    gPriceVector = BuildPriceVector(So, vol, dt, 2);
    
    % Linear Approximation
    Gamma = ((gVector(3)-gVector(2))/(gPriceVector(3)-gPriceVector(2))-...
        (gVector(2)-gVector(1))/(gPriceVector(2)-gPriceVector(1)))/...
        (0.5*(gPriceVector(3)-gPriceVector(1)));
end

function [gVector] = CalcGammaVector(CP, init, priceVector, P,...
    K, R, dt)
    % Returns a vector of option prices for two days into
    % the bionomial tree
    
    % opVector contains the opPrices for the next time step working back
    % from final day to option inception day
    opVector = zeros(length(priceVector)-1,1);
    for i = 1: length(priceVector)-1
        % init acts as a bool to determine if input is passing
        % initial stock prices or calculated option prices
        if(init == 1)
            opVector(i) = exp(-R * dt)* ...
                (P * CalcPayoff(CP, priceVector(i), K)...
                +(1-P)*CalcPayoff(CP, priceVector(i+1), K));
        else
            opVector(i) = exp(-R * dt)* ...
                (P * priceVector(i) + (1-P)* priceVector(i+1));
        end
    end

    % recursivly iterate through the function until tree converges to
    % 2 days after option inception
    if(length(opVector) > 3)
        gVector = CalcGammaVector(CP, 0, opVector, P, K, R, dt);
    else
        gVector = opVector;
    end
end

function [Rho] = CalcRho(AE,CP, So, K, R, Div, Vol, dt, n, gStep)
    % Shock rate up and down then calculate difference between resulting
    % option prices. Divide by rate change
    R1 = R*(1+gStep);
    R2 = R*(1-gStep);
    EffRate1 = R1 - Div;
    EffRate2 = R2 - Div;
    
    if (AE == 'A') 
        price1 = PriceAmericanOption(CP, So, K, R1, EffRate1, Vol, dt, n);
        price2 = PriceAmericanOption(CP, So, K, R2, EffRate2, Vol, dt, n);
    else
        price1 = PriceEuropeanOption(CP, So, K, R1, EffRate1, Vol, dt, n);
        price2 = PriceEuropeanOption(CP, So, K, R2, EffRate2, Vol, dt, n);
    end
    
    Rho = (price2 - price1)/(R1-R2);
end

function [Theta] = CalcTheta(AE,CP, So, K, R, EffRate, Vol, dt, n)
    % Evaluate theta of the option using linear approximation. Calculate
    % value of option at input time and at two time steps shorter. Divide
    % diff in prices by change in time.
    if (AE == 'A') 
        price1 = PriceAmericanOption(CP, So, K, R, EffRate, Vol, dt, n);
        price2 = PriceAmericanOption(CP, So, K, R, EffRate, Vol, dt, n-2);
    else
        price1 = PriceEuropeanOption(CP, So, K, R, EffRate, Vol, dt, n);
        price2 = PriceEuropeanOption(CP, So, K, R, EffRate, Vol, dt, n-2);
    end
    Theta = (price2 - price1)/(2*dt);
end

function [Vega] = CalcVega(AE,CP, So, K, R, EffRate, Vol, dt, n, gStep)
    % Shock vol price up and down by the given gStep amount
    Vol1 = Vol*(1+gStep);
    Vol2 = Vol*(1-gStep);
    
    if (AE == 'A') 
        price1 = PriceAmericanOption(CP, So, K, R, EffRate, Vol1, dt, n);
        price2 = PriceAmericanOption(CP, So, K, R, EffRate, Vol2, dt, n);
    else
        price1 = PriceEuropeanOption(CP, So, K, R, EffRate, Vol1, dt, n);
        price2 = PriceEuropeanOption(CP, So, K, R, EffRate, Vol2, dt, n);
    end
    
    Vega = (price2 - price1)/(Vol2-Vol1);
end








